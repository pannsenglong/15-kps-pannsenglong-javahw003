public class SalariedEmployee extends StaffMember implements Comparable<StaffMember>
{
    //current class variable declaration
    private double salary;
    private double bonus;

    //constructor
    public SalariedEmployee(int id, String name, String address, String STAFF_TYPE, double salary, double bonus) {
        super(id, name, address, STAFF_TYPE);
        this.salary = salary;
        this.bonus = bonus;
    }

    //getter and setter


    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }


    //override method
    @Override
    public String toString() {
        return "Salaried Staff:      " +
                "ID: " + id +
                ",      Name: " + name +
                ",      Address: " + address +
                ",      Base salary: " + salary + "$" +
                ",      Bonus: " + bonus + "$" +
                ",      Total Salary: " + pay() + "$";
    }

    @Override
    public double pay() {
        return this.salary+this.bonus;
    }

    @Override
    public int compareTo(StaffMember staff) {
        return this.name.compareTo(staff.name);
    }


}
