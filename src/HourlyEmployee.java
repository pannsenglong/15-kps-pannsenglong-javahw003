public class HourlyEmployee extends StaffMember implements Comparable<StaffMember>
{
    //current class variable declaration
    private int hoursWorked;
    private double rate;

    //constructor
    public HourlyEmployee(int id, String name, String address, String STAFF_TYPE, int hoursWorked, double rate) {
        super(id, name, address, STAFF_TYPE);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    //Getter and setter
    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    //override method
    @Override
    public String toString() {
        return "Hourly Staff:        " +
                "ID:" + id +
                ",      Name: " + name +
                ",      Address: " + address +
                ",      Hours Worked: " + hoursWorked + "h" +
                ",      Rate: " + rate +"$/hour" +
                ",      Total Salary: "+pay() + "$";

    }

    @Override
    public double pay() {
        return (this.hoursWorked * this.rate);
    }

    @Override
    public int compareTo(StaffMember staff) {
        return this.name.compareTo(staff.name);
    }

}
