
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.List;

public class MainLoading
{
    static Scanner scanner = new Scanner(System.in);
    public static void main(String args[])
    {
        int chooseOption;
        int chooseAddOption;
        MainLoading loading = new MainLoading();

        List<StaffMember> staffMembers = new ArrayList<>();

        while (true)
        {
            System.out.println("------- <<<  STAFF MANAGMENT  >>> -------");
            System.out.println("        1. Add Employee");
            System.out.println("        2. Edit Employee Information");
            System.out.println("        3. Remove Employee From List");
            System.out.println("        4. Display Employees list");
            System.out.println("        5. Exit The Program");
            System.out.println("-----------------------------------------\n");
            System.out.print("Choose your option: ");
            chooseOption = inputInt(1, 5);
            System.out.println("You choose option "+chooseOption+"\n");
            switch (chooseOption)
            {
                case 1:
                    loading.addNewStaff(chooseOption, staffMembers);
                    break;
                case 2:
                    loading.updateStaffInformation(staffMembers);
                    break;
                case 3:
                    loading.removeStaff(staffMembers);
                    break;
                case 4:
                    loading.displayStaffInformation(staffMembers);
                    break;
                case 5:
                    loading.exitTheProgram();
                    break;
            }
        }
    }

    //Add new staff to the list method =========================================================================
    void addNewStaff(int chooseAddOption, List<StaffMember> staffMembers)
    {
        System.out.println("There are 3 types of employee");
        System.out.println("1. Add volunteer employee");
        System.out.println("2. Add official employee");
        System.out.println("3. Add hourly employee");
        System.out.println("4. Back\n");
        System.out.print("==> Choose your option: ");
        chooseAddOption = inputInt(1,4);
        System.out.println("You choose option "+ chooseAddOption+ "\n");

        int id;
        String name;
        String address;
        String STAFF_TYPE;
        boolean isDuplecateId = false;
        switch (chooseAddOption)
        {
            case 1:
                System.out.print("Input employee ID: ");
                do {
                    isDuplecateId = false;
                    id = inputInt();
                    for (StaffMember staff : staffMembers) {
                        if (id == staff.getId()) {
                            System.out.println("\nSorry this ID has been taken");
                            System.out.print("Please input another ID: ");
                            isDuplecateId = true;
                            break;
                        }
                    }
                }while (isDuplecateId);
                System.out.print("Input employee name: ");
                scanner.nextLine();
                name = inputString();

                //convert to letter to upper case
                name = convertFirstLetterNameToUpperCase(name);
                System.out.print("Input employee address: ");
                address = inputString();
                STAFF_TYPE = "volunteerStaff";

                //add volunteer object to array list
                staffMembers.add(new Volunteer(id, name, address, STAFF_TYPE));
                System.out.println("==> Staff has been added successfully !!!");
                break;
            case 2:
                System.out.print("Input employee ID: ");
                do {
                    isDuplecateId = false;
                    id = inputInt();
                    for (StaffMember staff : staffMembers) {
                        if (id == staff.getId()) {
                            System.out.println("\nSorry this ID has been taken");
                            System.out.print("Please input another ID: ");
                            isDuplecateId = true;
                            break;
                        }
                    }
                }while (isDuplecateId);
                System.out.print("Input employee name: ");
                scanner.nextLine();
                name = inputString();

                //convert to letter to upper case
                name = convertFirstLetterNameToUpperCase(name);
                System.out.print("Input employee address: ");
                address = inputString();
                STAFF_TYPE = "officialStaff";
                System.out.print("Input employee Salary: ");
                double salary = inputDouble();
                System.out.print ("Input employee bonus: ");
                double bonus = inputDouble();

                //add volunteer object to array list
                staffMembers.add(new SalariedEmployee(id, name, address, STAFF_TYPE, salary , bonus));
                System.out.println("==> Staff has been added successfully !!!");
                break;
            case 3:
                System.out.print("Input employee ID: ");
                do {
                    isDuplecateId = false;
                    id = inputInt();
                    for (StaffMember staff : staffMembers) {
                        if (id == staff.getId()) {
                            System.out.println("\nSorry this ID has been taken");
                            System.out.print("Please input another ID: ");
                            isDuplecateId = true;
                            break;
                        }
                    }
                }while (isDuplecateId);
                System.out.print("Input employee name: ");
                scanner.nextLine();
                name = inputString();

                //convert to letter to upper case
                name = convertFirstLetterNameToUpperCase(name);

                System.out.print("Input employee address: ");
                address = inputString();
                STAFF_TYPE = "hourlyStaff";
                System.out.print("Input employee hours worked: ");
                int hoursWorked = scanner.nextInt();
                System.out.print("Input employee rate: ");
                double rate = inputDouble();

                //add volunteer object to array list
                staffMembers.add(new HourlyEmployee(id, name, address, STAFF_TYPE, hoursWorked , rate));
                System.out.println("==> Staff has been added successfully !!!");
                break;
            case 4:
                break;
        }
    }




    //Update staff information Method==========================================================================
    void updateStaffInformation(List<StaffMember> staffMembers)
    {
        if(staffMembers.size() <= 0)
        {
            System.out.println("==> Sorry list is empty now. ");
            System.out.println("    No staff to update information.\n");
            pressEnter();
        }
        else {
            int staffIdToEdit;
            int indexFound = 0;
            boolean isStaffFound = false;
            String staffType;
            System.out.println("----->>> UPDATE STAFF INFORMATION <<<-----");
            System.out.println("Enter staff's ID to edit information.");
            System.out.print("Input ID: ");
            staffIdToEdit = inputInt();

            for (int i = 0; i < staffMembers.size(); i++) {
                if (staffIdToEdit == staffMembers.get(i).getId()) {
                    isStaffFound = true;
                    indexFound = i;
                    break;
                }
            }
            staffType = staffMembers.get(indexFound).getSTAFF_TYPE();
            if (isStaffFound) {
                String newName;
                String newAddress;

                System.out.println("Staff with ID: " + staffIdToEdit + " is found.");
                switch (staffType) {
                    case "volunteerStaff":
                        System.out.println("Old information about this staff:\n");


                        System.out.println(staffMembers.get(indexFound).toString());
                        System.out.println("Input new information");
                        System.out.print("Name: ");
                        scanner.nextLine();
                        newName = inputString();

                        //convert to letter to upper case
                        newName = convertFirstLetterNameToUpperCase(newName);
                        System.out.print("Address: ");
                        newAddress = inputString();

                        //update volunteer staff information
                        staffMembers.set(indexFound, new Volunteer(staffMembers.get(indexFound).getId(), newName, newAddress, "volunteerStaff"));
                        break;
                    case "officialStaff":
                        System.out.println("Old information about this staff:");
                        System.out.println(staffMembers.get(indexFound).toString());

                        System.out.println("Input new information");
                        System.out.print("Name: ");
                        scanner.nextLine();
                        newName = inputString();

                        //convert to letter to upper case
                        newName = convertFirstLetterNameToUpperCase(newName);
                        System.out.print("Address: ");
                        newAddress = inputString();
                        System.out.print("Salary: ");
                        double newSalary = inputDouble();
                        System.out.print("Bonus: ");
                        double newBonus = inputDouble();

                        //update salary staff information
                        staffMembers.set(indexFound, new SalariedEmployee(staffMembers.get(indexFound).getId(), newName, newAddress, "officialStaff", newSalary, newBonus));
                        break;
                    case "hourlyStaff":
                        System.out.println("Old information about this staff:\n");
                        System.out.println(staffMembers.get(indexFound).toString());
                        System.out.println("\nInput new information");

                        //inputting new Information of hourly staff
                        System.out.print("Name: ");
                        scanner.nextLine();
                        newName = inputString();

                        //convert to letter to upper case
                        newName = convertFirstLetterNameToUpperCase(newName);
                        System.out.print("Address: ");
                        newAddress = inputString();
                        System.out.print("Hours Worked: ");
                        int newHoursWorked = scanner.nextInt();
                        System.out.println("Rate: ");
                        double newRate = inputDouble();

                        //update staff information
                        staffMembers.set(indexFound, new HourlyEmployee(staffMembers.get(indexFound).getId(), newName, newAddress, "hourlyStaff", newHoursWorked, newRate));
                }
            }
            else
            {
                System.out.println("\nSorry, the staff with ID: "+staffIdToEdit+" is not found.");
                System.out.println("Check ID again.\n");
            }
        }
    }





    // Remove staff from list Method ============================================================================
    void removeStaff(List<StaffMember> staffMembers)
    {
        //check that have staff ot not
        if(staffMembers.size() <= 0)
        {
            System.out.println("==> Sorry list is empty now. ");
            System.out.println("    No staff to remove from the list.\n");
            pressEnter();
        }
        else
        {
            // if have staff : input ID to remove
            System.out.println("----->>> REMOVE STAFF <<<-----");
            System.out.println("Enter staff's ID to remove from the list.");
            System.out.print("Input ID : ");
            int staffIdToRemove = inputInt();
            boolean isStaffFound = false;
            int indexStaffToRemove=0;
            for (int i=0; i<staffMembers.size(); i++) {
                //if found : we can remove
                if (staffIdToRemove == staffMembers.get(i).getId())
                {
                    isStaffFound = true;
                    indexStaffToRemove = i;
                    break;
                }
            }
            if (isStaffFound) {
                System.out.println("Staff with ID: " + staffIdToRemove + " is found.");
                System.out.println("Do you want to remove this staff from the list?");
                System.out.print("Press 'yes' to exit or press 'no' to cancel :");
                scanner.nextLine();
                String yesno = inputString();
                while (!yesno.equals("yes") && !yesno.equals("no")) {
                    System.out.println("You Inputted: " + yesno);
                    System.out.println("===============> WRONG KEY <================");
                    System.out.print("Press 'yes' to exit or press 'no' to cancel :");
                    yesno = scanner.next();
                }
                System.out.println("You inputted: " + yesno + "\n");
                if (yesno.equals("yes")) {
                    staffMembers.remove(indexStaffToRemove);
                    System.out.println("Staff has been remove successfully \n");
                    System.out.println("Thank you !!!");
                    pressEnter();
                } else {
                    System.out.println("Removing the staff has been cancelled.\n");
                    pressEnter();
                }
            }
            else
            {
                System.out.println("\nSorry, the staff with ID: "+staffIdToRemove+" is not found.");
                System.out.println("Check ID again.\n");
            }
        }
    }





    //Display Employee memtod ===================================================================================
    void displayStaffInformation(List<StaffMember> staffMembers)
    {
        System.out.println("-------------------------------------------------->>> DISPLAY EMPLOYEE <<<--------------------------------------------------\n");
        if(staffMembers.size() <=0 )
        {
            System.out.println("==> List is empty now. ");
            pressEnter();
        }
        else
        {
            System.out.println("Note: Staffs list has been sort by name.\n");
            Collections.sort(staffMembers, MainLoading.sortStaffByName);
            for (StaffMember staff: staffMembers)
            {
                System.out.println(staff.toString());
            }
            System.out.println("\n                                        ------------------------------------------\n");
            pressEnter();
        }
    }






    // Exit the program method =================================================================================
    void exitTheProgram()
    {
        // ask to make sure you want to exit or not
        System.out.println("Do you want to close the program?");
        System.out.print("Press 'yes' to exit or press 'no' to cancel :");
        String yesno=inputString();
        while(!yesno.equals("yes") && !yesno.equals("no"))
        {
            System.out.println("\nYou Inputted: "+yesno);
            System.out.println("===============> WRONG KEY <================");
            System.out.print("Press 'yes' to exit or press 'no' to cancel :");
            yesno=scanner.next();
        }
        System.out.println("You inputted: "+yesno+"\n");
        if(yesno.equals("yes"))
        {
            System.out.println("Program is closing...\n");
            System.out.println("The Program has been ended");
            System.out.println("Thank you !!!");
            System.exit(0);
        }
        else
        {
            System.out.println("Program is still continue...\n");
            pressEnter();
        }
    }






    // To sort array list by name =============================================================================
    public static Comparator<StaffMember> sortStaffByName = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember staff1, StaffMember staff2)
        {
            String name1 = staff1.getName();
            String name2 = staff2.getName();
            return name1.compareTo(name2);
        }
    };





    //Input Method for validate wrong input with limit range requirement =====================================
    static int inputInt(int lower, int upper)
    {
        int baseNum;
        while(true)
        {
            while(!scanner.hasNextInt())
            {
                if(!scanner.hasNextInt())
                {
                    scanner.next();
                    System.out.print("\tinvalid inputed, try again with positive number("+lower+"-"+upper+"): ");
                }
            }
            baseNum=scanner.nextInt();
            if(baseNum<lower || baseNum>upper || baseNum < 0)
            {
                System.out.print("\tinvalid inputed, try again with positive number("+lower+"-"+upper+"): ");
            }
            else
            {
                break;
            }
        }
        return baseNum;
    }





    //Input Method for validate wrong input without limit range requirement =====================================
    static int inputInt()
    {
        int baseNum;
        while(true)
        {
            while(!scanner.hasNextInt())
            {
                if(!scanner.hasNextInt())
                {
                    scanner.next();
                    System.out.print("\nInvalid inputted, try again with positive number: ");
                }
            }
            baseNum=scanner.nextInt();
            if(baseNum < 0)
            {
                System.out.print("\nInvalid inputted, try again with positive number: ");
            }
            else
            {
                break;
            }
        }
        return baseNum;
    }






    //Inputting method that validate the wrong string inputted ===================================================
    static String inputString()
    {
        boolean isBoolean   ;
        String name;
        do
        {
            String usernamePattern = "[a-zA-Z ]+";
            name = scanner.nextLine();
            isBoolean = name.matches(usernamePattern);
            if (!isBoolean)
            {
                System.out.print("\nPlease input correct name(a-z,A-Z): ");
                isBoolean=false;
            }
        } while (!isBoolean);
        return name;
    }






    //Inputting method that validate the wrong double inputted
    static double inputDouble()
    {
        double baseNum;
        while(true)
        {
            while(!scanner.hasNextDouble())
            {
                if(!scanner.hasNextDouble())
                {
                    scanner.next();
                    System.out.print("\nInvalid salary inputted, any incorrect. Check it again. ");
                    System.out.print("Input salary again: ");
                }
            }
            baseNum=scanner.nextDouble();
            if(baseNum < 0)
            {
                System.out.println("\nInvalid inputted, try again with positive number: ");
                System.out.print("Input salary again: ");
            }
            else
            {
                break;
            }
        }
        return baseNum;
    }






    //Press Enter Key Method ===================================================================================
    static void pressEnter()
    {
        System.out.println("\t\n>>> Press \"ENTER\" to continue...");
        try {
            int read = System.in.read(new byte[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println();
    }






    //Convert first letter of name to upper case
    static String convertFirstLetterNameToUpperCase(String name)
    {
        String firstLetter="";
        firstLetter = Character.toString(name.charAt(0)).toUpperCase();
        return firstLetter + name.substring(1);
    }
}
