
public class Volunteer extends StaffMember implements Comparable<StaffMember>
{
    //constructor
    public Volunteer(int id, String name, String address, String STAFF_TYPE) {
        super(id, name, address, STAFF_TYPE);
    }

    //overrided method
    @Override
    public double pay() {
        return 0;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int compareTo(StaffMember staff) {
        return this.name.compareTo(staff.name);
    }
}
