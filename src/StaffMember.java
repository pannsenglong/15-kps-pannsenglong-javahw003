public abstract class StaffMember
{
    //variable declaration
    protected int id;
    protected String name;
    protected String address;
    protected String STAFF_TYPE;

    //constructor
    public StaffMember(int id, String name, String address, String STAFF_TYPE)
    {
        this.id = id;
        this.name = name;
        this.address = address;
        this.STAFF_TYPE = STAFF_TYPE;
    }

    //getter and setter


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSTAFF_TYPE() {
        return STAFF_TYPE;
    }

    public void setSTAFF_TYPE(String STAFF_TYPE) {
        this.STAFF_TYPE = STAFF_TYPE;
    }

    //method declaration
    @Override
    public String toString() {
        return "Volunteer Staff:     " +
                "ID: " + id +
                ",      Name: " + name +
                ",      Address: " + address;
    }

    public abstract  double pay();
}
